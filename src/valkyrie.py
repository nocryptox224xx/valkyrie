#!/usr/bin/env python3
"""
This script contains the logic for managing a stake pool on the Cardano
blockchain consisting of multiple Jormungandr nodes hosted on separate servers.
The logic performs restarts when necessary as well as promotes the healthiest
node to leader status (to produce blocks).

This application requires the remote pool nodes follow some specific
conventions regaurding setup and file locations. This information is defined
in the Valkyrie configuration file.

Copyright (c) 2020 Viper Science

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
"""


__version__ = "0.1.1"


import json
import pytz
import sched
import time
import yaml
from datetime import datetime
from multiprocessing.dummy import Pool as ThreadPool
from pathlib import Path
from valkyrie_lib import JormungandrNode, PoolToolIO, EpochLogger


class Valkyrie():
    """Jormangandr stake pool manager.
    """

    def __init__(self, config_path, log_dir):
        self.logger = EpochLogger(log_dir)  # Do this first!
        self.load_config(config_path)  # Initializes self.config

        # The node bundle stores important state information such as tip,
        # epoch, slots, etc. It is serialized to persist state accross a
        # restart of Valkyrie. If no bundle file is found, defaults are
        # returned.
        self.load_bundle()  # Initializes self.bundle

        # Build the list of node objects from information in the config file.
        self.nodes = []
        for i, h in enumerate(self.config["hosts"]):
            node = JormungandrNode(
                idnum=i,
                host=h["host"],
                user=h["user"],
                ssh_key_file=h["sshkeyfile"],
                ssh_port=h["sshport"],
                jormungandr_path=h["jorpath"],
                jcli_path=h["jclipath"],
                storage_path=h["storagepath"],
                secret_path=h["secretpath"],
                config_path=h["configpath"],
                rest_port=h["restport"],
                genesis_hash=self.config["blockchain"]["genhash"],
                pool_id=self.config["blockchain"]["poolid"]
            )
            self.nodes.append(node)

        # Create pooltool.io interface
        self.pooltool = PoolToolIO(
            self.config["pooltool"]["userid"],
            self.config["blockchain"]["poolid"],
            self.config["blockchain"]["genhash"],
            self.config["manager"]["storage"]
            )

        # Create a thread pool for communicating with the nodes in parallel.
        self.thread_pool = ThreadPool(4)

    def load_config(self, file_path):
        """Load the Valkyrie configuration file (YAML).
        """
        self.logger.info(f"loading configuration file: {file_path}")
        with open(file_path, 'r') as file_object:
            self.config = yaml.safe_load(file_object)
            self.logger.verbose(json.dumps(self.config))

    def load_bundle(self):
        """Load the Valkyrie state information (defaults if doesn't exist).
        """
        file_name = "valkyrie-bundle.yaml"
        file_path = Path(self.config["manager"]["storage"]) / file_name
        if file_path.is_file():
            self.logger.info(f"Loading bundle: {file_path}")
            with open(file_path, 'r') as file_object:
                self.bundle = yaml.safe_load(file_object)
            self.logger.debug(f"Loaded bundle: {self.bundle}")
        else:
            self.bundle = {"tip": -1, "epoch": None, "slots": {}, "stake": 0}
            self.logger.debug(f"Using default bundle: {self.bundle}")

    def save_bundle(self):
        """Save the Valkyrie state information in the storage directory.
        """
        file_name = "valkyrie-bundle.yaml"
        file_path = Path(self.config["manager"]["storage"]) / file_name
        with open(file_path, 'w') as file_obj:
            self.logger.debug(f"Dumping bundle {self.bundle} to: {file_path}")
            yaml.dump(self.bundle, file_obj, explicit_start=True, indent=2)

    def run(self):
        """Run Valkyrie (startup and monitor the pool).
        """

        # Startup - open an SSH connection to each node and start a new
        # instance of Jormungandr.
        results = self.thread_pool.map(
            lambda n: (n, self._node_startup(n)),
            self.nodes
        )
        running_nodes = []
        for node, is_up in results:
            if is_up:
                running_nodes.append(node)
            else:
                self.logger.warn(f"Unable to start node {node.id}.")
                self.logger.warn(f"Excluding node {node.id} from node list.")
        self.nodes = running_nodes

        # Setup the scheduler and use it to monitor the nodes.
        self.scheduler = sched.scheduler(time.time, time.sleep)
        self.scheduler.enter(0.0, 1, self._monitor_pool)
        self.scheduler.run()

    def shutdown(self):
        """Stop Valkyrie (shutdown all pool nodes).
        """
        def shutdown_node(node):
            self.logger.info(f"Shutting down node {node.id}")
            node.stop_jormungandr()
            node.conn.close()
        self.thread_pool.map(shutdown_node, self.nodes)
        self.thread_pool.terminate()

    def _node_startup(self, node):
        """Startup a Jormungandr node and get its status.
        """
        self.logger.info(f"Starting node: {node.id}")
        # Using restart kills any other Jormungandr processes.
        node.restart_jormungandr(leader=True)
        info = self._update_node_status(node)
        return(info["state"].lower() != "error")

    def _monitor_pool(self):
        """The main monitoring method. Update the status of each pool and then
        take appropriate action to promote the healthiest node and log
        important statistics and information.
        """

        # Flags
        epoch_change = False

        # Query the status of each node in parallel.
        self.thread_pool.map(self._update_node_status, self.nodes)

        # Find the leader node and update the pool status accordingly.
        for node in self.nodes:
            if node.state == "running" and node.leader:

                # Update the epoch - check for a change
                epoch_change = self._update_epoch(node)

                # Update tip
                self._update_tip(node)

                # Update leader logs
                self._update_leader_logs(node, flush=epoch_change)

                # Update pool stats
                self._update_pool_stats(node, new_epoch=epoch_change)

        if epoch_change:

            # On epoch change, restart all non-leader nodes.
            self.logger.info(
                "Detected epoch rollover. Restarting non-leader nodes.",
                self.bundle["epoch"]
            )
            restart = [n for n in self.nodes if not n.leader]
            self._restart_nodes(restart)

        else:

            # Prioritize the nodes and take appropriate actions.
            promote, demote, restart = self._prioritize_nodes(self.nodes)
            self._promote_nodes(promote)
            self._demote_nodes(demote)
            self._restart_nodes(restart)

            # If a slot is scheduled in the very near future, pause monitoring
            # so that we don't have a leadership change during block
            # production.
            if self._slot_is_imminent():
                self.logger.info("Scheduled slot is imminent. Pausing...")
                time.sleep(1.5*self.config["manager"]["slotbuffer"])

        # Schedule the monitoring function to repeat at the specified interval.
        delay = self.config["manager"]["delay"]
        self.scheduler.enter(delay, 1, self._monitor_pool)

    def _update_node_status(self, node):
        """Have the node update its status and then log the information.
        """
        status = node.get_status()
        self.logger.verbose(f"Node {node.id} status: {json.dumps(status)}")
        if node.state == "running":
            chain_height = status["lastBlockHeight"]
            block_date = status["lastBlockDate"]
            block_hash = status["lastBlockHash"]
            jvers = status["version"].split()[1]
            leader_status = 1 if node.leader else 0
            msg = (
                f"Node {node.id}: height={chain_height}, "
                f"leader={leader_status}, date={block_date}, "
                f"hash={block_hash}, state={node.state}, "
                f"uptime={node.uptime}s, connections={node.n_connections}, "
                f"version={jvers}"
            )
            self.logger.info(msg)
        else:
            msg = f"Node {node.id}: Uptime={node.uptime}s, state={node.state}"
            self.logger.info(msg)
        return status

    def _update_epoch(self, node):
        """Check for an epoch change.
        """
        # Note: The bunde epoch must start as None so that an epoch change is
        # not triggered on first startup.
        epoch_change = False
        epoch = int(float(node.raw_status["lastBlockDate"]))
        prev_epoch = self.bundle["epoch"]
        if not prev_epoch or epoch > prev_epoch:
            epoch_change = epoch > prev_epoch if prev_epoch else False
            self.bundle["epoch"] = epoch
            self.save_bundle()  # Save the updated bundle
        return epoch_change

    def _update_tip(self, node, epoch=None):
        """ Update the tip and notify pooltool.io.
        """
        tip = node.block_height
        if tip > self.bundle["tip"]:
            self.bundle["tip"] = tip
            self.save_bundle()  # Save the updated bundle
            # Send leader's tip to pooltool.io and log the result.
            result = self.pooltool.sendmytip_v3(node)
            epoch = int(float(node.raw_status["lastBlockDate"]))
            msg = f"Sent tip {tip} to pooltool.io. Response = {result}"
            self.logger.info(msg, epoch)
        return tip

    def _update_leader_logs(self, node, flush=False):
        """Look through the leader logs (pulled from a running node) and make
        sure to update the bundle with any changes.
        """
        raw_logs = node.get_leader_logs()

        # Flush out the saved slots. This should be done when a new epoch
        # starts.
        if flush:
            self.bundle["slots"] = {}
            self.save_bundle()  # Save the updated bundle

        # Old logs may be present in the node status. Only process leader slots
        # from the current epoch.
        logs = [log for log in raw_logs if
                log["scheduled_at_date"].startswith(str(self.bundle["epoch"]))]

        # Use this flag to determine if we should send new slots to pooltool.
        new_slots = False

        # Look through the leader logs and make sure we save any new slots
        # (should happen when a new epoch starts) or update the status of any
        # slots (should happen when we make a block).
        for slot in logs:
            slot_date = slot["scheduled_at_date"]
            if slot_date in self.bundle["slots"]:
                saved_slot = self.bundle["slots"][slot_date]
                if (slot["status"] != saved_slot["status"] and not
                        (isinstance(saved_slot["status"], dict) and
                            "Block" in saved_slot["status"])):
                    self.logger.verbose(f"Updated slot status: {slot}")
                    slot_status = json.dumps(slot["status"])
                    self.logger.info(
                        f"Updated status for slot {slot_date}"
                        f": {slot_status}"
                    )
                    self.bundle["slots"][slot_date] = slot
                    self.save_bundle()  # Save the updated bundle
            else:
                self.logger.verbose(f"Found new scheduled slot: {slot}")
                self.logger.info(
                    f"""Found new slot {slot["scheduled_at_date"]} """
                    f"""scheduled at time {slot["scheduled_at_time"]}"""
                )
                self.bundle["slots"][slot_date] = slot
                self.save_bundle()  # Save the updated bundle
                new_slots = True

        # If new slots were found (new epoch started), send slots to pooltool.
        if new_slots:
            result = self.pooltool.send_slots(node, mode="gpg")
            msg = f"Reporting slots to pooltool.io. Response = {result}"
            self.logger.info(msg)

    def _update_pool_stats(self, node, new_epoch=False):
        """Have the node update its status and then log the information.
        """
        pool_stats = node.get_pool_stats()
        if "total_stake" in pool_stats:
            stake = float(pool_stats["total_stake"])/1e6  # Convert to ADA
            if stake != self.bundle["stake"]:
                self.logger.info(
                    f"Total stake changed from {self.bundle['stake']:,} "
                    f"to {stake:,}"
                )
                self.bundle["stake"] = stake
                self.save_bundle()  # Save the updated bundle
        if new_epoch:
            # Log last epoch reward stats
            self.logger.verbose(
                "rewards: " + json.dumps(pool_stats['rewards']))

    def _prioritize_nodes(self, nodes):
        """Determine which nodes should be leaders, which should stop being
        leaders, or which need to be restarted.
        """
        promote = []
        demote = []
        restart = []

        max_dist = 15  # TODO read this from config
        max_repeat = self.config["manager"]["maxblockrepeat"]

        def same_chain(node, leaders):
            # Determine if a node is on the same chain as one of the leaders.
            return True  # TODO

        # Only sort running nodes
        running_nodes = [n for n in nodes if n.state == "running"]
        if len(running_nodes) > 1:

            # Sort the nodes by block height. Break any ties by keeping the
            # current leader.
            running_nodes.sort(
                key=lambda x: (x.block_height, x.id), # changed to keep EU node as leader if possible
                reverse=True
            )

            promote.append(running_nodes.pop(0))
            for node in running_nodes:
                if (node.block_height < (promote[0].block_height - max_dist)
                        and node.state_repeats > max_repeat):
                    restart.append(node)
                elif same_chain(node, promote):
                    demote.append(node)
                else:
                    promote.append(node)

        elif len(running_nodes) == 1:
            promote = running_nodes

        # Check on nodes that are not currently running.
        for node in nodes:

            # Check for nodes that are stuck bootstrapping
            if node.state == "bootstrapping":
                max_bs_repeat = self.config["manager"]["maxbootstraprepeat"]
                if node.state_repeats > max_bs_repeat:
                    self.logger.warn(
                        f"Found Node {node.id} stuck bootstrapping. "
                        "Attempting to restart Jormungandr."
                    )
                    restart.append(node)

            # Attempt to restart nodes that may have died (error state)
            if node.state == "error" and node.state_repeats > 1:
                self.logger.warn(
                    f"Found Node {node.id} in error state. "
                    "Attempting to restart Jormungandr."
                )
                restart.append(node)

        # If there are no nodes flagged to restart, check to make sure one of
        # the non-leader nodes has not repeated its block too many times. This
        # will help catch situations where all nodes have stalled at the same
        # time.
        if not restart:
            restart.extend([n for n in demote if n.state_repeats > max_repeat])
            restart.extend([n for n in promote if n.state_repeats > max_repeat])

        return (promote, demote, restart)

    def _promote_nodes(self, nodes):
        """Promote the given nodes to leader.
        """
        def promote(node):
            self.logger.debug(f"Node {node.id} is leader")
            if not node.leader:
                self.logger.info(f"Promote node {node.id} to leader")
                node.make_leader()
        self.thread_pool.map(promote, nodes)

    def _demote_nodes(self, nodes):
        """Demote (if needed) the specified nodes.
        """
        def demote(node):
            self.logger.debug(f"Node {node.id} is not leader")
            if node.leader:
                self.logger.info(f"Demote node {node.id}")
                node.delete_leader()
        self.thread_pool.map(demote, nodes)

    def _restart_nodes(self, nodes):
        """Restart the Jormungandr process on the specified nodes.
        """
        def restart(node):
            self.logger.info(f"Restart node {node.id}")
            node.restart_jormungandr(leader=True)
        self.thread_pool.map(restart, nodes)

    def _slot_is_imminent(self):
        """Iterate through the slots scheduled for the current epoch and return
        True if any of them occur within the specified buffer. Otherwise,
        return false.
        """
        for slot in self.bundle["slots"].values():
            sched_time = slot["scheduled_at_time"]
            st = datetime.strptime(sched_time, "%Y-%m-%dT%H:%M:%S+00:00")
            st = st.replace(tzinfo=pytz.UTC)
            td = (st - datetime.now(pytz.UTC)).total_seconds()
            if td > 0 and td < self.config["manager"]["slotbuffer"]:
                return True  # Return on first one found
        return False


if __name__ == "__main__":
    print(f"Running Valkyrie {__version__}")

    # Path to configuration file (YAML format).
    config_path = "../config/viper-valkyrie-config.yaml"

    # Path to the log file directory.
    log_path = "../storage/"

    # Create and initialize Valkyrie object.
    v = Valkyrie(config_path, log_path)

    # Run Valkyrie and handle shutdown
    try:
        v.run()
    except KeyboardInterrupt:
        # The program can only be stopped with a keyboard interrupt. When this
        # occurs, shut down jormungandr on the nodes.
        print()  # Put the ^C on its own line
        v.shutdown()
        print("Good bye ADA my lovelace!!")
