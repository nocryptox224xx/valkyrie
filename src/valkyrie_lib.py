"""
Copyright (c) 2020 Viper Science

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
"""


import json
import os
import requests
import secrets
import shutil
import subprocess
import sys
import time
from collections import deque
from fabric import Connection
from hashlib import sha256
from loguru import logger
from pathlib import Path


class JormungandrNode():
    """Defines and interface for interacting with and controlling a Jormungandr
    node on the cardano blockchain.
    """

    def __init__(self, idnum, host, user, ssh_key_file, ssh_port,
                 jormungandr_path, jcli_path, storage_path, secret_path,
                 config_path, rest_port, genesis_hash, pool_id):
        self.id = idnum
        self.conn = Connection(
            host=host,
            user=user,
            connect_kwargs={
                "key_filename": ssh_key_file,
                "banner_timeout": 200,
            },
        )
        self.conn.open()
        self.jorpath = jormungandr_path
        self.jclipath = jcli_path
        self.storage_path = storage_path
        self.secret_path = secret_path
        self.config_path = config_path
        self.rest_port = rest_port
        self.genesis_hash = genesis_hash
        self.pool_id = pool_id
        self.recent_blocks = deque("", 5)
        self.block_height = 0
        self.state_repeats = 0
        self.uptime = 0
        self.state = ""
        self.verbose = False
        self.leader = False
        self.n_connections = 0
        self.raw_status = None

    def __debug_info(self, message):
        if self.verbose:
            print(message)

    def __reset_state(self):
        self.recent_blocks = deque("", 5)
        self.block_height = 0
        self.state_repeats = 0
        self.uptime = 0

    def start_jormungandr(self, leader=False):
        if leader:
            jcmd = (
                f"{self.jorpath} --genesis-block-hash {self.genesis_hash}"
                f" --config {self.config_path} --secret {self.secret_path}"
            )
        else:
            jcmd = (
                f"{self.jorpath} --genesis-block-hash {self.genesis_hash}"
                f" --config {self.config_path}"
            )
        cmd = f"nohup {jcmd} &> /dev/null &"
        self.__debug_info(jcmd)
        self.conn.run(cmd, disown=True)  # Start the node and disown
        self.__reset_state()
        self.leader = leader
        time.sleep(1)

    def stop_jormungandr(self, use_pkill=True):
        if use_pkill:
            self.conn.run("pkill jormungandr", warn=True, hide=True)
        else:
            cmd = (f"{self.jclipath} rest v0 shutdown get "
                   f"--host http://127.0.0.1:{self.rest_port}/api")
            res = self.conn.run(cmd, warn=True, hide=True)
            time.sleep(1)
            # If jcli can't kill the node (maybe its bootstrapping), then kill
            # the process directly.
            if "success" not in res.stdout.lower():
                self.stop_jormungandr(use_pkill=True)

    def restart_jormungandr(self, leader=False, delete_storage=False):
        self.stop_jormungandr()
        if delete_storage:
            sp = self.storage_path
            self.conn.run(f"rm -rf {sp} & mkdir {sp}", warn=True, hide=True)
        time.sleep(5)
        self.start_jormungandr(leader)

    def is_leader(self):
        # Determine if the node is currently set as a leader node.
        #
        # Example jcli command:
        # jcli rest v0 leaders get -h http://127.0.0.1:3100/api
        cmd = (f"{self.jclipath} rest v0 leaders get "
               f"--host http://127.0.0.1:{self.rest_port}/api")
        # paramiko.ssh_exception.SSHException: Channel closed.
        try:
            res = self.conn.run(cmd, warn=True, hide=True)
            self.leader = "1" in res.stdout
        except: #paramiko.ssh_exception.SSHException:
            time.sleep(5)
            self.conn.close()
            self.conn.open()
            res = self.conn.run(cmd, warn=True, hide=True)
            self.leader = "1" in res.stdout
        return self.leader

    def make_leader(self):
        # Make the node a leader (able to produce blocks).
        #
        # Example jcli command:
        # jcli rest v0 leaders post -f secret.yml -h http://127.0.0.1:3100/api
        cmd = (f"{self.jclipath} rest v0 leaders post -f {self.secret_path} "
               f"--host http://127.0.0.1:{self.rest_port}/api")
        res = self.conn.run(cmd, warn=True, hide=True)
        self.__debug_info(res.stdout.strip())
        self.leader = "1" in res.stdout
        return self.leader  # Successfull if true

    def delete_leader(self):
        # Demote node to passive (no longer will produce blocks).
        #
        # Example jcli command:
        # jcli rest v0 leaders delete 1 -h http://127.0.0.1:3100/api
        cmd = (f"{self.jclipath} rest v0 leaders delete 1 "
               f"--host http://127.0.0.1:{self.rest_port}/api")
        res = self.conn.run(cmd, warn=True, hide=True)
        self.__debug_info(res.stdout.strip())
        self.leader = not ("success" in res.stdout.lower())
        return not self.leader  # Successful if return true

    def get_leader_logs(self):
        # Query the leader logs.
        #
        # Example jcli command:
        # jcli rest v0 leaders log get -h http://127.0.0.1:3100/api
        try:
            cmd = (f"{self.jclipath} rest v0 leaders logs get "
                   f"--output-format json "
                   f"--host http://127.0.0.1:{self.rest_port}/api")
            res = self.conn.run(cmd, warn=True, hide=True)
            j = json.loads(res.stdout)
        except Exception:
            j = []
        return j

    def get_status(self):
        try:
            cmd = (f"{self.jclipath} rest v0 node stats get "
                   f"--output-format json "
                   f"--host http://127.0.0.1:{self.rest_port}/api")
            res = self.conn.run(cmd, warn=True, hide=True)
            j = json.loads(res.stdout)
        except Exception:
            j = {"state": "error"}
        self.raw_status = j

        # Also, update the number of connections and the leader status.
        self.get_num_connections()
        self.is_leader()

        # If the node has changed state, save the new state and zero the repeat
        # counter so we can know when the node is stuck.
        new_state = self.state != j["state"].lower()
        if new_state:
            self.state = j["state"].lower()
            self.state_repeats = 0

        # If the node is running, clear the repeat counter when a new block is
        # found and increment the repeat counter when new blocks are not found.
        # This gives info to upstream logic to determine if the node is stuck.
        # Additionally, if a new block is found, update the recent blocks
        # queue.
        if self.state == "running":
            new_block = self.block_height != int(j["lastBlockHeight"])
            if new_block:
                self.block_height = int(j["lastBlockHeight"])
                self.recent_blocks.appendleft(j["lastBlockHash"])
                self.uptime = float(j["uptime"])
                self.state_repeats = 0
            else:
                self.state_repeats += 1
        else:
            # If we are repeating a non-running state, increment the repeat
            # counter so upstream logic can determine if we need to restart.
            if not new_state:
                self.state_repeats += 1

        return j

    def get_block(self, block_hash):
        try:
            cmd = (f"{self.jclipath} rest v0 block {block_hash} get "
                   f"--host http://127.0.0.1:{self.rest_port}/api")
            res = self.conn.run(cmd, warn=True, hide=True)
            return res.stdout.strip()
        except Exception:
            return None

    def get_num_connections(self):
        cmd = "ss -np4 state synchronized | grep pid=`pgrep jormungan` | wc -l"
        try:
            res = self.conn.run(cmd, warn=True, hide=True)
            self.n_connections = int(res.stdout.strip())
        except Exception:
            self.n_connections = -1
        return self.n_connections

    def get_pool_stats(self):
        """Get pool information like stake, rewards last epoch, taxes, etc.
        """
        cmd = (f"{self.jclipath} rest v0 stake-pool get {self.pool_id} "
               f"--output-format json "
               f"--host http://127.0.0.1:{self.rest_port}/api")
        try:
            res = self.conn.run(cmd, warn=True, hide=True)
            return json.loads(res.stdout)
        except Exception:
            return {}


###############################################################################


class PoolToolIO():
    """Provide an interface to the pooltool.io API.
    """

    def __init__(self, userid, poolid, genhash, keydir):
        self.userid = userid
        self.poolid = poolid
        self.genhash = genhash
        self.keydir = Path(keydir)

    def __get(self, url, params=None):
        """ Query specified URL and return JSON contents
        """
        r = requests.get(url, params=params)
        if r.status_code != 200:
            return None
        else:
            return r.json()

    def sendmytip_v1(self, pool):

        # Query information from the pool (Jormungandr)
        if pool.raw_status is None:
            # Update status from the pool if its not already there.
            pool.get_status()
        if pool.raw_status["state"].lower() != "running":
            return None
        last_block_height = pool.raw_status["lastBlockHeight"]

        # Build the url
        url = (f"https://api.pooltool.io/v0/sharemytip?poolid={self.poolid}"
               f"&userid={self.userid}&genesispref={self.genhash}"
               f"&mytip={last_block_height}")

        return self.__get(url)

    def sendmytip_v2(self, pool):

        # Query information from the pool (Jormungandr)
        if pool.raw_status is None:
            # Update status from the pool if its not already there.
            pool.get_status()
        if pool.raw_status["state"].lower() != "running":
            return None
        last_block_height = pool.raw_status["lastBlockHeight"]
        last_block_hash = pool.raw_status["lastBlockHash"]
        last_block = pool.get_block(last_block_hash)
        last_pool_id = last_block[168:232]

        # Build the url
        url = (f"https://api.pooltool.io/v0/sharemytip?poolid={self.poolid}"
               f"&userid={self.userid}&genesispref={self.genhash}"
               f"&mytip={last_block_height}&lasthash={last_block_hash}"
               f"&lastpool={last_pool_id}")

        return self.__get(url)

    def sendmytip_v3(self, pool):

        platform_name = "Valkyrie by Viper Science"

        # Query information from the pool (Jormungandr)
        if pool.raw_status is None:
            # Update status from the pool if its not already there.
            pool.get_status()
        if pool.raw_status["state"].lower() != "running":
            return None
        last_block_height = pool.raw_status["lastBlockHeight"]
        last_block_hash = pool.raw_status["lastBlockHash"]
        last_block = pool.get_block(last_block_hash)
        last_pool_id = last_block[168:232]
        last_parent = last_block[104:168]
        last_slot = "0x" + last_block[24:32]
        last_epoch = "0x" + last_block[16:24]
        jorm_version = pool.raw_status["version"]
        params = {
            "platform": platform_name,
            "jormver": jorm_version
        }

        # Build the url
        url = (f"https://api.pooltool.io/v0/sharemytip?poolid={self.poolid}"
               f"&userid={self.userid}&genesispref={self.genhash}"
               f"&mytip={last_block_height}&lasthash={last_block_hash}"
               f"&lastpool={last_pool_id}&lastparent={last_parent}"
               f"&lastslot={last_slot}&lastepoch={last_epoch}")

        return self.__get(url, params=params)

    def send_slots(self, pool, mode="gpg"):
        """
        """

        # Query status information from the pool (Jormungandr)
        raw_status = pool.get_status()
        if raw_status["state"].lower() != "running":
            return None

        # Get the epoch numbers
        current_epoch = int(float(raw_status["lastBlockDate"]))
        previous_epoch = current_epoch - 1

        # Get the scheduled slots
        slots = [log for log in pool.get_leader_logs()
                 if log["scheduled_at_date"].startswith(str(current_epoch))]
        num_slots = len(slots)
        slots_json = json.dumps(slots)

        if mode == "gpg":
            # Generating symmetric key for current epoch and retrieving
            # previous epoch key.
            prev_file_path = self.keydir / f"passphrase_{previous_epoch}"
            if os.path.exists(prev_file_path):
                with open(prev_file_path) as prev_file:
                    previous_epoch_key = prev_file.read()
            else:
                previous_epoch_key = ""
            key_file_path = self.keydir / f"passphrase_{current_epoch}"
            if os.path.exists(key_file_path):
                with open(key_file_path, 'r') as key_file:
                    current_epoch_key = key_file.read()
            else:
                current_epoch_key = secrets.token_urlsafe(32)
                with open(key_file_path, 'w') as key_file:
                    key_file.write(current_epoch_key)

            # Encrypting current slots for sending to pooltool.
            cmd = (f"gpg --symmetric --armor --batch --passphrase "
                   f"{current_epoch_key}")
            res = subprocess.run(cmd.split(), input=slots_json,
                                 stdout=subprocess.PIPE,
                                 universal_newlines=True)
            current_slots_encrypted = res.stdout.strip()

            # Creating JSON for sending to pooltool
            data_dict = {
                "currentepoch": current_epoch,
                "poolid": self.poolid,
                "genesispref": self.genhash,
                "userid": self.userid,
                "assigned_slots": num_slots,
                "previous_epoch_key": previous_epoch_key,
                "encrypted_slots": current_slots_encrypted
            }

        elif mode == "hash":
            # Pushing the current slots to file and getting the slots from the
            # last epoch.
            prev_file_path = self.keydir / f"leader_slots_{previous_epoch}"
            if os.path.exists(prev_file_path):
                with open(prev_file_path) as prev_file:
                    last_epoch_slots = prev_file.read()
            else:
                last_epoch_slots = ""
            slots_file_path = self.keydir / f"leader_slots_{current_epoch}"
            if not os.path.exists(slots_file_path):
                with open(slots_file_path, 'w') as slots_file:
                    slots_file.write(json.dumps(slots))

            current_epoch_hash = sha256(slots_json.encode('utf-8')).hexdigest()

            data_dict = {
                "currentepoch": current_epoch,
                "poolid": self.poolid,
                "genesispref": self.genhash,
                "userid": self.userid,
                "assigned_slots": num_slots,
                "this_epoch_hash": current_epoch_hash,
                "last_epoch_slots": last_epoch_slots
            }

        else:
            data_dict = {
                "currentepoch": current_epoch,
                "poolid": self.poolid,
                "genesispref": self.genhash,
                "userid": self.userid,
                "assigned_slots": num_slots
            }

        data_json = json.dumps(data_dict)

        # Send the data using a http post.
        api_url = "https://api.pooltool.io/v0/sendlogs"
        headers = {
            "content-type": "application/json",
            "accept": "application/json"
        }
        r = requests.post(api_url, data=data_json, headers=headers)
        return r.json()


###############################################################################


class EpochLogger():
    """Process stake pool data and log to file for post processing. Log files
    are rotated per epoch.
    """

    def __init__(self, log_dir, epoch=None, terminal=True):
        self.log_dir = Path(log_dir)
        self.epoch = epoch
        self.terminal = terminal

        # Open the log file.
        self.file_path = self.log_dir / "valkyrie.log"

        # Setup the logger
        logger.remove()
        logger.level("VERBOSE", no=15)
        if terminal:
            logger.add(
                sys.stderr,
                level="INFO",
                format=(
                    "<green>{time:YYYY-MM-DD HH:mm:ss}</green> "
                    "| <level>{level}</level> "
                    "| <cyan>{name}</cyan> - <level>{message}</level>"
                ),
                enqueue=True,
                colorize=True
            )
        logger.add(
            self.file_path,
            level="TRACE",
            format="{time:YYYY-MM-DD HH:mm:ss} | {level} | {name} - {message}",
            enqueue=True,
            serialize=False,
            colorize=False
        )
        self.logger = logger.patch(
            lambda record: record.update(name="valkyrie"))

    def __rotate(self, epoch):
        # If self.epoch is None, we are just writing to the default log file so
        # continue that. Once we have a change from one epoch number to the
        # next we will rotate the logs.
        if self.epoch is not None:
            log_archive = self.log_dir / f"valkyrie_epoch{self.epoch}.log"
            if log_archive.is_file():
                # Move the current log file contents to the archived version.
                with open(log_archive, 'a') as archive_file:
                    with open(self.file_path, 'r') as log_file:
                        archive_file.write(log_file.read())
            else:
                shutil.copyfile(self.file_path, log_archive)
            open(self.file_path, 'w').close()  # clear log file
        self.epoch = epoch

    def trace(self, message, epoch=None):
        if epoch and epoch > (self.epoch if self.epoch else 0):
            self.__rotate(epoch)
        self.logger.trace(message)

    def debug(self, message, epoch=None):
        if epoch and epoch > (self.epoch if self.epoch else 0):
            self.__rotate(epoch)
        self.logger.debug(message)

    def verbose(self, message, epoch=None):
        if epoch and epoch > (self.epoch if self.epoch else 0):
            self.__rotate(epoch)
        self.logger.log("VERBOSE", message)

    def info(self, message, epoch=None):
        if epoch and epoch > (self.epoch if self.epoch else 0):
            self.__rotate(epoch)
        self.logger.info(message)

    def success(self, message, epoch=None):
        if epoch and epoch > (self.epoch if self.epoch else 0):
            self.__rotate(epoch)
        self.logger.success(message)

    def warn(self, message, epoch=None):
        if epoch and epoch > (self.epoch if self.epoch else 0):
            self.__rotate(epoch)
        self.logger.warning(message)

    def error(self, message, epoch=None):
        if epoch and epoch > (self.epoch if self.epoch else 0):
            self.__rotate(epoch)
        self.logger.error(message)

    def critical(self, message, epoch=None):
        if epoch and epoch > (self.epoch if self.epoch else 0):
            self.__rotate(epoch)
        self.logger.critical(message)
