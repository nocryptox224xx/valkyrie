# Valkyrie

> This repository is exclusively a project archive.

Valkyrie is a Jormungandr stake pool manager designed to run multiple remote nodes communicated with via SSH.
The nodes are all part of the same stake pool, and Valkyrie automates the process of promoting healthy nodes to leader while restarting sick nodes.

## Setup

Clone the repository

    git clone https://gitlab.com/viper-stake-pool/valkyrie.git

Create a Python 3 virtual environment

    cd valkyrie
    python3 -m venv venv/
    source venv/bin/activate

Install dependencies

    pip install -r  requirements.txt

Add SSH keys

    mkdir sshkeys
    # copy .pem files here

Update the valkarie config file for connecting to the nodes

    ---
    hosts:
    - host: ec2-18-236-67-217.us-west-2.compute.amazonaws.com
        user: ubuntu
        sshport: 22
        sshkeyfile: ../sshkeys/cardano-node-us-west.pem
        jorpath: /home/ubuntu/bin/jormungandr
        jclipath: /home/ubuntu/bin/jcli
        storagepath: /home/ubuntu/node/storage
        configpath: /home/ubuntu/node/config.yaml
        secretpath: /home/ubuntu/node/secret.yaml
        restport: 3100

Run `valkyrie.py`

    cd src
    python valkyrie.py